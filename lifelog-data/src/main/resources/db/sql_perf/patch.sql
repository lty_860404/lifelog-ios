
create table FAMILY_EVENT (
	EVENT_ID bigint(20),
	FAMILY_ID bigint(20),
	index FAM_EVE_EVE_ID_IDX (EVENT_ID),
	foreign key (EVENT_ID)
		references EVENT(ID)
		on delete cascade
) engine = innodb;