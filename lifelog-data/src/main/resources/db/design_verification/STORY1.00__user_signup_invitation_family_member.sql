START TRANSACTION;
--  Story step 1
insert into FAMILY ( ID, NAME ) 
	values (1, 'Luora\'s family');
insert into USER (ID, FAMILY_ID,USER_ID, USER_NAME, PASSWORD, PHONE_NUMBER,
				 AVATOR_PICTURE, REGISTERED_DATETIME)
	values ( 1, 1, 'Luora1234', 'Luora', 'password', '+819000000000', 
				'/avator/default.png', '2013-12-24 10:00:00');
insert into FAMILY_MEMBER (FAMILY_ID, USER_ID, RELATIONSHIP, STATUS) 
	values ( 1, 1, 'Me', 1);
-- Story step 2 
insert into PHOTO ( ID, OWNER_ID, CONTENT_TYPE, LATEST_UPDATE_TIME, UPLOAD_TIME,
				 OWNER_COMMENT, FILEPATHS) 
	values ( 1, 1, 'image/jpeg', '2013-12-24 12:30:00', '2013-12-24 12:30:00', 
				'My First Photo', '/s3/pohot1.jpg');
-- Story step 3
insert into FAMILY_MEMBER_INVITATION (PROMOTER_ID, PROMOTER_FAMILY_ID,
				 RELATION_SHIP, INVITEE_PHONE_NUMBER, INVITEE_USER_NAME)
	values ( 1, 1,
				 'Husband', 'John', '+819000000001');
insert into EXTERNAL_INVITATION ( PROMOTER_ID, INVITEE_PHONE_NUMBER) 
	values ( 1, '+819000000001');
-- Story step 4
insert into FAMILY ( ID, NAME ) 
	values (2, 'John\'s family');
insert into USER (ID, FAMILY_ID,USER_ID, USER_NAME, PASSWORD, PHONE_NUMBER, 
				 AVATOR_PICTURE, REGISTERED_DATETIME)
	values ( 2, 2, 'John1234', 'John', 'password', '+819000000001',
				'/avator/default.png', '2013-12-24 12:50:05');
insert into FAMILY_MEMBER (FAMILY_ID, USER_ID, RELATIONSHIP, STATUS) 
	values ( 2, 2, 'Me', 1);
update EXTERNAL_INVITATION 
	set STATUS = 1 
	where INVITEE_PHONE_NUMBER = '+819000000001';
-- Story step 5
insert into FAMILY_MEMBER (FAMILY_ID, USER_ID, RELATIONSHIP, STATUS) 
	values ( 2, 1, 'Wife', 1);
insert into FAMILY_MEMBER (FAMILY_ID, USER_ID, RELATIONSHIP, STATUS) 
	values ( 1, 2, 'Husband', 1);
update FAMILY_MEMBER_INVITATION 
	set STATUS = 1 
	where PROMOTER_ID = 1 
		and INVITEE_PHONE_NUMBER = '+819000000001';
-- Story step 6
insert PHOTO_LIKED (PHOTO_ID, BE_LIKED_BY)
	values ( 1, 2);
update PHOTO 
	set BELIKED_NUM=BELIKED_NUM+1, 
		BELIKED = concat('John@John1234,',BELIKED) 
	where ID = 1;
-- Story step 7
insert PHOTO_COMMENT (PHOTO_ID, COMMENTED_BY, COMMENT) 
	values ( 1, 1, 'Very Nice!');
update PHOTO 
	set LATEST_COMMENT= concat('John@John1234:Very Nice!,', LATEST_COMMENT) 
	where ID = 1;
-- Story step 8
insert into PHOTO ( OWNER_ID, CONTENT_TYPE, LATEST_UPDATE_TIME, UPLOAD_TIME,
				 OWNER_COMMENT, FILEPATHS) 
	values ( 2, 'image/jpeg', '2013-12-24 13:30:00', '2013-12-24 13:30:00',
				 'Cool Photo, is\'nt it?', '/s3/pohot1.jpg');
COMMIT;
