-- Tina's timeline
select p.*
    from PHOTO p  
        left join FAMILY_MEMBER fm
             on p.OWNER_ID = fm.USER_ID and fm.FAMILY_ID = 1
        left join EVENT_PHOTO_STREAM eps
            on p.ID = eps.PHOTO_ID
        left join EVENT_ATTENDEE eat 
            on eat.EVENT_ID = eps.EVENT_ID and eat.ATTENDEE_ID = 1 and eat.IS_OWNER = 1
    where fm.FAMILY_ID = 1 or eat.ATTENDEE_ID = 1 
    order by p.LATEST_UPDATE_TIME desc
    limit 0, 10;
-- John's timeline
select p.*
    from PHOTO p  
        left join FAMILY_MEMBER fm
             on p.OWNER_ID = fm.USER_ID and fm.FAMILY_ID = 2
        left join EVENT_PHOTO_STREAM eps
            on p.ID = eps.PHOTO_ID
        left join EVENT_ATTENDEE eat 
            on eat.EVENT_ID = eps.EVENT_ID and eat.ATTENDEE_ID = 2 and eat.IS_OWNER = 1
    where fm.FAMILY_ID = 2 or eat.ATTENDEE_ID = 2 
    order by p.LATEST_UPDATE_TIME desc
    limit 0, 10;

-- Tina's timeline
select p.*
    from PHOTO p  
        left join FAMILY_MEMBER fm
             on p.OWNER_ID = fm.USER_ID and fm.FAMILY_ID = 3
        left join EVENT_PHOTO_STREAM eps
            on p.ID = eps.PHOTO_ID
        left join EVENT_ATTENDEE eat 
            on eat.EVENT_ID = eps.EVENT_ID and eat.ATTENDEE_ID = 3 and eat.IS_OWNER = 1
    where fm.FAMILY_ID = 3 or eat.ATTENDEE_ID = 3 
    order by p.LATEST_UPDATE_TIME desc
    limit 0, 10;

-- Event's PHOTO Stream
select * from PHOTO
	where ID in (
			select PHOTO_ID from EVENT_PHOTO_STREAM where EVENT_ID = 1
		);