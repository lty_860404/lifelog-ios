START TRANSACTION;
-- STORY step 1
insert into EVENT ( ID, EVENT_TYPE_CODE, EVENT_NAME, 
					BEGIN_DATETIME, END_DATETIME)
	values ( 1, 1, 'Luora & John Wedding', 
			'2014-01-03 00:00:00', '2014-01-03 23:59:59');
insert into EVENT_ATTRS (EVENT_ID, ATTR_NAME, ATTR_VALUE)
	values ( 1, 'Bride father name', 'Bush');
insert into EVENT_ATTRS (EVENT_ID, ATTR_NAME, ATTR_VALUE)
	values ( 1, 'groom father name', 'Martin');
insert into EVENT_ATTRS (EVENT_ID, ATTR_NAME, ATTR_VALUE)
	values ( 1, 'Address', 'address info');
insert into EVENT_ATTENDEE (EVENT_ID, ATTENDEE_ID, 
							ATTENDEE_NAME, ATTENDEE_PHONE_NUMBER, IS_OWNER, STATUS)
	values (1, 1,
			'Luora','+819000000001',1, 1);
insert into EVENT_ATTENDEE (EVENT_ID, ATTENDEE_ID, 
							ATTENDEE_NAME, ATTENDEE_PHONE_NUMBER, IS_OWNER, STATUS)
	values (1,2,
			'John','+819000000002',1, 1);
-- STORY step 1
COMMIT;
