//
//  BSLTabBarController.m
//  Lifelog
//
//  Created by tianyin luo on 14-3-17.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BSLTabBarController.h"

@interface BSLTabBarController ()

@end

@implementation BSLTabBarController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
       
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tabBar setTintColor:[UIColor redColor]];
    [self.tabBar setSelectedImageTintColor:[UIColor blueColor]];
    
    //获取原来定义的viewControllers,这里只需要它的tabbar就行了
    NSArray *viewControllers = [self viewControllers];
    
    UINavigationController *timelineNav = [[UINavigationController alloc] init];
    [timelineNav setTabBarItem:[(UIViewController *)[viewControllers objectAtIndex:0] tabBarItem]];
    UINavigationController *eventsNav = [[UINavigationController alloc] init];
    [eventsNav setTabBarItem:[(UIViewController *)[viewControllers objectAtIndex:1] tabBarItem]];
    UINavigationController *cameraNav = [[UINavigationController alloc] init];
    [cameraNav setTabBarItem:[(UIViewController *)[viewControllers objectAtIndex:2] tabBarItem]];
    
    UIStoryboard *familiesStory = [UIStoryboard storyboardWithName:@"Family" bundle:nil];
    UINavigationController *familiesNav = [familiesStory instantiateInitialViewController];
    [familiesNav setTabBarItem:[(UIViewController *)[viewControllers objectAtIndex:3] tabBarItem]];
    
    UIStoryboard *personalStory = [UIStoryboard storyboardWithName:@"PersonalInfo" bundle:nil];
    UINavigationController *personalNav = [personalStory instantiateInitialViewController];
    [personalNav setTabBarItem:[(UIViewController *)[viewControllers objectAtIndex:4] tabBarItem]];
    
    [self setViewControllers:[NSArray arrayWithObjects:timelineNav,eventsNav,cameraNav,familiesNav,personalNav,nil]
                    animated: NO];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
