//
//  BSLContractsViewController.h
//  Lifelog
//
//  Created by tianyin luo on 14-3-20.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBookUI/AddressBookUI.h>

@interface BSLContractListViewController : ABPeoplePickerNavigationController
@end
