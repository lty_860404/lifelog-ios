//
//  BSLSearchCell.h
//  Lifelog
//
//  Created by tianyin luo on 14-3-20.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSLSearchCell : UITableViewCell

//    @property (strong,nonatomic) IBOutlet UISearchDisplayController *searchBar;
        @property (strong,nonatomic) IBOutlet UISearchBar *searchBar;

    -(IBAction)backgroundTap:(id)sender;
@end
