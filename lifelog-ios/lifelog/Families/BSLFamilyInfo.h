//
//  BSLFamilyInfo.h
//  Lifelog
//
//  Created by tianyin luo on 14-3-19.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSLFamilyInfo : NSObject

    @property (strong,nonatomic) NSString *photoUrl;
    @property (strong,nonatomic) NSString *userName;
    @property (strong,nonatomic) NSString *relationDesc;
    @property (strong,nonatomic) NSString *ID;
    @property (nonatomic) BOOL inviteFlag;

    @property (strong,nonatomic) NSString *personalSignature;
    @property (strong,nonatomic) NSString *mobileNum;

@end
