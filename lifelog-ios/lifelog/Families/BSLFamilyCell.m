//
//  BSLFamilyCell.m
//  Lifelog
//
//  Created by tianyin luo on 14-3-20.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BSLFamilyCell.h"

@implementation BSLFamilyCell

@synthesize detailLabel;
@synthesize titleLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.imageView setFrame:CGRectMake(5, 5, 60, 60)];
        self.imageView.layer.masksToBounds = YES;
        self.imageView.layer.cornerRadius=self.imageView.frame.size.width/2;
        
        self.titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(80, (self.contentView.frame.size.height/2)-20, (self.contentView.frame.size.width-self.imageView.frame.size.width-10), 20)];
        [self.contentView addSubview:self.titleLabel];
        
        self.detailLabel = [[UILabel alloc]initWithFrame:CGRectMake(85, (self.contentView.frame.size.height/2)+5, (self.contentView.frame.size.width-self.imageView.frame.size.width-10), 20)];
        [self.contentView addSubview:self.detailLabel];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    [self.imageView setFrame:CGRectMake(5, 5, 60, 60)];
    self.imageView.layer.masksToBounds = YES;
    self.imageView.layer.cornerRadius=self.imageView.frame.size.width/2;
    
    self.titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(80, (self.contentView.frame.size.height/2)-20, (self.contentView.frame.size.width-self.imageView.frame.size.width-10), 20)];
    [self.contentView addSubview:self.titleLabel];
    
    self.detailLabel = [[UILabel alloc]initWithFrame:CGRectMake(85, (self.contentView.frame.size.height/2)+5, (self.contentView.frame.size.width-self.imageView.frame.size.width-10), 20)];
    [self.contentView addSubview:self.detailLabel];
}


-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self.imageView setFrame:CGRectMake(5, 5, 60, 60)];
        self.imageView.layer.masksToBounds = YES;
        self.imageView.layer.cornerRadius=self.imageView.frame.size.width/2;
        
        [self.textLabel setFrame:CGRectMake(75, 30, (self.contentView.frame.size.width-self.imageView.frame.size.width-10), 20)];
        
        self.detailLabel = [[UILabel alloc]initWithFrame:CGRectMake(90, 50, (self.contentView.frame.size.width-self.imageView.frame.size.width-10), 20)];
        [self.contentView addSubview:self.detailLabel];
    }
    return self;
}

//等比例缩放
-(UIImage *)getImage:(UIImage *)image
           withWidth:(CGFloat)width
           andHeight:(CGFloat)height
{
    UIImage * resultImage = image;
    if (image.size.width != width && image.size.height != height)
    {
        CGSize itemSize = CGSizeMake(width,height);
        UIGraphicsBeginImageContext(itemSize);
        CGRect imageRect = CGRectMake(0, 0, width, height);
        [image drawInRect:imageRect];
        resultImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    return resultImage;
}

//设置照片
- (void) setPhoto:(UIImage *)image{
    UIImage* resultImage = [self getImage:image
                                withWidth:self.imageView.frame.size.width
                                andHeight:self.imageView.frame.size.height];
    [self.imageView setImage:resultImage];
}

@end
