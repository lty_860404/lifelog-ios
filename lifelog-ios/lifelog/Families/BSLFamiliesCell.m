//
//  BSLFamiliesCell.m
//  Lifelog
//
//  Created by tianyin luo on 14-3-19.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BSLFamiliesCell.h"

@implementation BSLFamiliesCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
