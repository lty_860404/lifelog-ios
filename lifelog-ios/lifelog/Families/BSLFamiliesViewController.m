//
//  BSLFamiliesViewController.m
//  Lifelog
//
//  Created by tianyin luo on 14-3-19.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BSLFamiliesViewController.h"
#import "BSLFamilyActionCell.h"
#import "BSLFamilyInfo.h"

@interface BSLFamiliesViewController ()

@end

@implementation BSLFamiliesViewController

NSArray* familiesData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (void) refreshFamiliesData
{
    BSLFamilyInfo *wanzi = [[BSLFamilyInfo alloc] init];
    [wanzi setPhotoUrl:@"Avatar-Init.png" ];
    [wanzi setUserName:@"丸子"];
    [wanzi setRelationDesc:@"老婆"];
    [wanzi setID:@"@wanzi"];
    [wanzi setInviteFlag:YES];
    
    BSLFamilyInfo *balu = [[BSLFamilyInfo alloc] init];
    [balu setPhotoUrl:@"Avatar-Init.png" ];
    [balu setUserName:@"巴鲁"];
    [balu setRelationDesc:@"兄弟"];
    [balu setID:@"@balu"];
    [balu setInviteFlag:NO];
    
    BSLFamilyInfo *jinjin = [[BSLFamilyInfo alloc] init];
    [jinjin setPhotoUrl:@"Avatar-Init.png" ];
    [jinjin setUserName:@"金金"];
    [jinjin setRelationDesc:@""];
    [jinjin setID:@"@jinjin"];
    [jinjin setInviteFlag:NO];
    
    BSLFamilyInfo *kaba = [[BSLFamilyInfo alloc] init];
    [kaba setPhotoUrl:@"Avatar-Init.png" ];
    [kaba setUserName:@"卡爸"];
    [kaba setRelationDesc:@"爸爸"];
    [kaba setID:@"@kaba"];
    [kaba setInviteFlag:YES];
    
    BSLFamilyInfo *kamu = [[BSLFamilyInfo alloc] init];
    [kamu setPhotoUrl:@"Avatar-Init.png" ];
    [kamu setUserName:@"卡母"];
    [kamu setRelationDesc:@"妈妈"];
    [kamu setID:@"@kaMu"];
    [kamu setInviteFlag:YES];
    
    familiesData = [NSArray arrayWithObjects:wanzi,balu,jinjin,kaba,kamu, nil];
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (familiesData == nil || familiesData.count == 0){
        [self refreshFamiliesData];
    }
    return familiesData.count;
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}

-(BSLFamilyActionCell *) loadFamilyData:(BSLFamilyInfo *)familyInfo
                             ToFamilyCell:(BSLFamilyActionCell *)cell
{
    UIImage *photo = [UIImage imageNamed:familyInfo.photoUrl];
    [cell setPhoto:photo];
    [cell.titleLabel setText:[[NSString alloc] initWithFormat:@"%@ (%@)", familyInfo.userName, familyInfo.relationDesc ]];
    [cell.detailLabel setText:familyInfo.ID];
    [cell setInviteFlag:familyInfo.inviteFlag];
    return cell;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BSLFamilyActionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"member" forIndexPath:indexPath ];
    if(cell == nil){
        cell = [[BSLFamilyActionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"member"];
    }
    if(cell.textLabel.text == nil || [cell.textLabel.text isEqual: @""]){
        if (familiesData == nil || familiesData.count == 0){
            [self refreshFamiliesData];
        }
        BSLFamilyInfo *data = [familiesData objectAtIndex:indexPath.row ];
        cell = [self loadFamilyData:data ToFamilyCell:cell ];
    }
    return cell;
}


@end
