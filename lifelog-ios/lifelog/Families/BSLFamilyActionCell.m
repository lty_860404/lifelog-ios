//
//  BSLFamiliesCell.m
//  Lifelog
//
//  Created by tianyin luo on 14-3-19.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BSLFamilyActionCell.h"

@implementation BSLFamilyActionCell

@synthesize actionBtn;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.actionBtn setBackgroundColor:[UIColor purpleColor]];
        [self.actionBtn.layer setCornerRadius:2];
        [self.actionBtn setTitle:@"已确认" forState:UIControlStateNormal];
    }
    return self;
}




-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self.actionBtn setBackgroundColor:[UIColor purpleColor]];
        [self.actionBtn.layer setCornerRadius:2];
        [self.actionBtn setTitle:@"已确认" forState:UIControlStateNormal];
    }
    return self;
}

-(void) setInviteFlag:(Boolean)flag
{
//    CGFloat frameWidth = self.contentView.frame.size.width;
//    CGFloat frameHeight =self.contentView.frame.size.height;
    UIColor *backgroundColor = [UIColor yellowColor];
    NSString *title = @"重发邀请";
    
    if(flag){
        title = @"已确认";
        backgroundColor = [UIColor greenColor];
    }
    
//    CGFloat btnWidth = [self getLabelLength:title];
    
//    self.actionBtn = [[UIButton alloc] initWithFrame:CGRectMake((frameWidth-btnWidth-20), (frameHeight/2+10), btnWidth+10, 20)];
    [self.actionBtn setBackgroundColor:backgroundColor];
    [self.actionBtn.layer setCornerRadius:2];
    [self.actionBtn setTitle:title forState:UIControlStateNormal];
    [self.actionBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    if (!flag){
        [self.actionBtn addTarget:self action:@selector(inviteAgain:) forControlEvents:UIControlEventTouchDown];
    }
    
//    [self.contentView addSubview:self.actionBtn];
}

-(IBAction) inviteAgain:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"重新邀请"
                                                   message:@"邀请成功"
                                                  delegate:self
                                         cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                         otherButtonTitles:nil];
    [alert show];
}

-(CGFloat)getLabelLength:(NSString *)strString{
    CGSize labsize = [strString sizeWithFont:[UIFont systemFontOfSize:16] constrainedToSize:CGSizeMake(275, 9999) lineBreakMode:NSLineBreakByCharWrapping];
    return labsize.width;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
}

@end
