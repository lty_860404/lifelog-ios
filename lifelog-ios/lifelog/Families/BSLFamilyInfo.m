//
//  BSLFamilyInfo.m
//  Lifelog
//
//  Created by tianyin luo on 14-3-19.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BSLFamilyInfo.h"

@interface BSLFamilyInfo ()

@end

@implementation BSLFamilyInfo

@synthesize photoUrl;
@synthesize userName;
@synthesize relationDesc;
@synthesize ID;
@synthesize inviteFlag;
@synthesize personalSignature;
@synthesize mobileNum;

@end
