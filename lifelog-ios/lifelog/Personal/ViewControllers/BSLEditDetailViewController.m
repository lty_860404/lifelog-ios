//
//  BIEditDetailViewController.m
//  lifelog
//
//  Created by tianyin luo on 14-2-20.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BSLEditDetailViewController.h"
#import "BSLPersonalInfoViewController.h"

@interface BSLEditDetailViewController ()
- (void)configureView;
@end

@implementation BSLEditDetailViewController

@synthesize detailName;
@synthesize detailText;
@synthesize detailNameLabel;
@synthesize detailTextField;

#pragma mark - Managing the detail item

- (void)setDetailName:(id)newDetailName andValue:(id)newDetailText
{
    if (detailName != newDetailName) {
        detailName = newDetailName;
        detailText = newDetailText;
        [self configureView];
    }
}

- (void)configureView
{
    if (detailName) {
        detailNameLabel.text = detailName;
    }
    if (detailText) {
        detailTextField.text = detailText;
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.detailTextField.delegate = self;
    [self configureView];
    
//    if(self.view){
//        [(UIControl *)self.view addTarget:self action:@selector(backgroundTap:) forControlEvents:UIControlEventTouchDown];
//    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)keyboardWillShow:(NSNotification *)noti
{
    //键盘输入的界面调整
    //键盘的高度
    float height = 216.0;
    CGRect frame = self.view.frame;
    frame.size = CGSizeMake(frame.size.width, frame.size.height - height);
    [UIView beginAnimations:@"Curl"context:nil];//动画开始
    [UIView setAnimationDuration:0.30];
    [UIView setAnimationDelegate:self];
    [self.view setFrame:frame];
    [UIView commitAnimations];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    // When the user presses return, take focus away from the text field so that the keyboard is dismissed.
    NSTimeInterval animationDuration = 0.30f;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    CGRect rect = CGRectMake(0.0f, 20.0f, self.view.frame.size.width, self.view.frame.size.height);
    self.view.frame = rect;
    [UIView commitAnimations];
    [textField resignFirstResponder];
    return YES; 
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGRect frame = textField.frame;
    int offset = frame.origin.y + 32 - (self.view.frame.size.height - 216.0 - 108.0);//键盘高度216
    NSTimeInterval animationDuration = 0.30f;
    [UIView beginAnimations:@"ResizeForKeyBoard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    float width = self.view.frame.size.width;
    float height = self.view.frame.size.height;
    if(offset > 0){
        CGRect rect = CGRectMake(0.0f, -offset,width,height);
        self.view.frame = rect;
    } 
    [UIView commitAnimations]; 
}

-(IBAction)backgroundTap:(id)sender
{
    // When the user presses return, take focus away from the text field so that the keyboard is dismissed.
    NSTimeInterval animationDuration = 0.30f;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    CGRect rect = CGRectMake(0.0f, 20.0f, self.view.frame.size.width, self.view.frame.size.height);
    self.view.frame = rect;
    [UIView commitAnimations];
    [self.detailTextField resignFirstResponder];
}

- (IBAction)updateSelectDetail:(id)sender
{
    NSString *detailText=self.detailTextField.text;
    NSInteger count = [self.navigationController.viewControllers count];
    BSLPersonalInfoViewController *userInfoController = (BSLPersonalInfoViewController *)[self.navigationController.viewControllers objectAtIndex:(count-2)];
    [userInfoController updateDetailForSelectedRow:detailText];
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction) navBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
