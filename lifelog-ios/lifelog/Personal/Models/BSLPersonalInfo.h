//
//  BSLPersonalInfo.h
//  Lifelog
//
//  Created by tianyin luo on 14-3-17.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BSLPersonalInfo : NSObject

    @property (strong,nonatomic) NSString *photoUrl;
    @property (strong,nonatomic) NSString *userName;
    @property (strong,nonatomic) NSString *personalSignature;
    @property (strong,nonatomic) NSString *ID;
    @property (strong,nonatomic) NSString *mobileNum;

@end
