//
//  BIUserInfoViewController.h
//  lifelog
//
//  Created by tianyin luo on 14-2-20.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BIUserInfoViewController : UITableViewController<UITableViewDataSource,UITableViewDelegate>

    @property (strong,nonatomic) NSArray *keys;

    -(void) updateDetailForSelectedRow:(NSString *)detailText;
    -(IBAction)popUpToSelectImage:(id)sender;
@end
