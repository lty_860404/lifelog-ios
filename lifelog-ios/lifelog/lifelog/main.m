
//  main.m
//  lifelog
//
//  Created by tianyin luo on 14-2-20.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BIAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BIAppDelegate class]));
    }
}
