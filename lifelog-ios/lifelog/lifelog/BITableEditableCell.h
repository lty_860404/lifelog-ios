//
//  BITableEditableCell.h
//  lifelog
//
//  Created by tianyin luo on 14-2-24.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BITableEditableCell : UITableViewCell

    @property (strong, nonatomic) id valueItem;

    @property (weak, nonatomic) IBOutlet UILabel *valueLabel;

@end
