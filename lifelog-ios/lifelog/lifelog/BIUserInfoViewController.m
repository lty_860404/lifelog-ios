//
//  BIUserInfoViewController.m
//  lifelog
//
//  Created by tianyin luo on 14-2-20.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BIUserInfoViewController.h"
#import "BIEditDetailViewController.h"
#import "BITableSwitchCell.h"

@interface BIUserInfoViewController ()

@end

@implementation BIUserInfoViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.keys = @[@"照片",@"名字",@"个性签名",@"",@"ID",@"ID允许搜索"];
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 4;
    }else if(section == 1){
        return self.keys.count-4;
    }else{
        return 0;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"";
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section==0 && indexPath.row == 0){
        return 150;
    }else{
        return 50;
    }
}

-(IBAction)popUpToSelectImage:(id)sender{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"选择头像" message:@"请选择头像:" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:nil];
    // optional - add more buttons:
    //[alert addButtonWithTitle:@"确认"];
    UITableViewController *tableView = [[UITableViewController alloc] initWithStyle:UITableViewStylePlain ];
    [tableView 
    [alert addSubview: tableView];
    [alert show];
}


-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (indexPath.section==0 && indexPath.row == 0){
        cell = [tableView dequeueReusableCellWithIdentifier:@"image"forIndexPath:indexPath];
        cell.userInteractionEnabled = YES;
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(popUpToSelectImage:) ];
        singleTap.numberOfTapsRequired = 1;
        singleTap.numberOfTouchesRequired = 1;
        [cell addGestureRecognizer:singleTap];
//        [(UIControl *)cell.backgroundView addTarget:self action:@selector(popUpToSelectImage:) forControlEvents:UIControlEventTouchDown];
        UIImage *image = [UIImage imageNamed:@"pikaqiu.png"];
        cell.imageView.image = image;
    } else if (indexPath.section==0 && indexPath.row == 1){
        cell = [tableView dequeueReusableCellWithIdentifier:@"Editable"forIndexPath:indexPath];
        cell.textLabel.text = self.keys[indexPath.row];
        cell.detailTextLabel.text = @"思远";
    } else if (indexPath.section==0 && indexPath.row == 2){
        cell = [tableView dequeueReusableCellWithIdentifier:@"cell"forIndexPath:indexPath];
        cell.textLabel.text = self.keys[indexPath.row];
    } else if (indexPath.section==0 && indexPath.row == 3){
        cell = [tableView dequeueReusableCellWithIdentifier:@"Editable"forIndexPath:indexPath];
        cell.detailTextLabel.text = @"lol";
    } else if (indexPath.section==1 && indexPath.row == 0){
        cell = [tableView dequeueReusableCellWithIdentifier:@"Editable"forIndexPath:indexPath];
        cell.textLabel.text = self.keys[4+indexPath.row];
        cell.detailTextLabel.text = @"siyuan";
    } else if (indexPath.section==1 && indexPath.row == 1){
        cell = [tableView dequeueReusableCellWithIdentifier:@"cell"forIndexPath:indexPath];
        UISwitch *switchview = [[UISwitch alloc] initWithFrame:CGRectMake(0,0,30,10) ];
        [switchview addTarget:self action:@selector(switchIDSearch:) forControlEvents:UIControlEventValueChanged];
        cell.accessoryView = switchview;
        cell.textLabel.text = self.keys[4+indexPath.row];
    }
    return cell;
}

/*ID允许搜索 按钮的方法绑定*/
-(void) switchIDSearch:(id)sender
{
    UISwitch *switchView = (UISwitch *)sender;
    if ([switchView isOn]){
        NSLog(@"on");
    }else{
        NSLog(@"off");
    }
}

- (void) updateDetailForSelectedRow:(NSString *)detailText
{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    cell.detailTextLabel.text = detailText;
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        NSString* key = cell.textLabel.text;
        NSString* value = cell.detailTextLabel.text;
        [[segue destinationViewController] setDetailName:key andValue: value];
    }
}

@end
