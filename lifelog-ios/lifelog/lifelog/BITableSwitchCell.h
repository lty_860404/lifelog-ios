//
//  BITableSwitchCell.h
//  lifelog
//
//  Created by tianyin luo on 14-2-26.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BITableSwitchCell : UITableViewCell

    @property (weak, nonatomic) IBOutlet UILabel *textLabel;
    @property (weak, nonatomic) IBOutlet UISwitch *switchBtn;

@end
