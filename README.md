## How to build projects

### Prepare database
Lifelog project uses mysql as its database, so you need install a mysql
on localhost or use a remote database.

Modify the following properties in the file gradle.properties
 - jdbcDriver
 - jdbcURL
 - jdbcUser
 - jdbcPassword

#### create a init database

> gradle flywayMigrate

### Build the REST Services

### Build the iOS application